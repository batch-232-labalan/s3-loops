package com.zuitt.example;

import java.util.Arrays;
import java.util.HashMap;


public class Loops {
    public static void main(String[] args) {

//        /* WHILE LOOP */
//
//        int a = 1;
//        while(a < 5){
//            System.out.println("While Loop Counter: " + a);
//            a++;
//        }
//
//        // ***
//        boolean hasNoInput = true;
//        while(hasNoInput){
//
//            System.out.println("Enter your name:");
//            String name = userInput.nextLine();
//
//            if(name.isEmpty()){
//                System.out.println("Please try again.");
//            } else {
//                hasNoInput = false;
//                System.out.println("Thank you for your input!");
//            }
//
//        }
//
//        /* DO-WHILE LOOP */
//
//        int b = 5;
//        do {
//            System.out.println("Countdown: " + b);
//            b--;
//        } while (b > 1);
//
//        /* FOR LOOP */
//
//        /* SIMPLE FOR LOOP */
//
//        for(int i = 1; i <= 10; i++){
//            System.out.println("Count: " + i);
//        }

        /* FOR LOOP OVER AN ARRAY */

        int[] intArray = {100,200,300,400,500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println("Item at index number " + i + " is " + intArray[i]);
        }

        /* LOOP OVER A MULTIDIMENSIONAL ARRAY */

        String[][] classroom = new String[3][3];

        //first Row
        classroom[0][0] = "Tony";
        classroom[0][1] = "Steve";
        classroom[0][2] = "Bruce";

        //second Row
        classroom[1][0] = "Natasha";
        classroom[1][1] = "Wanda";
        classroom[1][2] = "Clint";

        //third Row
        classroom[2][0] = "Scott";
        classroom[2][1] = "Peter";
        classroom[2][2] = "Stephen";

        //nested for loops
        for(int row = 0; row < 3; row++){
            for (int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }
        System.out.println(Arrays.deepToString(classroom));

        //enhanced for loop
        //In java, you can use enhance for loop to loop over each item in an array or array list
        //for each in java

        String[] members = {"Eugene","Vincent","Dennis","Alfred"};
        for(String member: members){
            System.out.println(member);
        }

       //for each(javascript) res: members.forEach(member => console.log(member));

        for(String[] row: classroom){
//            System.out.println(Arrays.toString(row));
            for(String student: row){
                System.out.println(student);
            }
        }

        //forEach for hashmap

        HashMap<String,String> techniques = new HashMap<>();
        techniques.put(members[0],"Spirit Gun");
        techniques.put(members[1],"Black Dragon");
        techniques.put(members[2],"Rose Whip");
        techniques.put(members[3],"Spirit Sword");
        System.out.println(techniques);

        //requires lambda expression as an argument
        //lambda expression - short block of code which takes in parameter and returns a value
        techniques.forEach((key,value) -> {
            System.out.println("Member " + key + " uses " + value);
        });




    }
}
