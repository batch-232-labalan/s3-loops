package com.zuitt.example;

import java.util.Scanner;

public class ExcemptionHandling {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        int num = 0;
        System.out.println("Input a number:");


        try{
            num = input.nextInt();
        } catch (Exception e){
            System.out.println("Invalid Input");
            e.printStackTrace();
        }
        System.out.println("The number you entered is " + num);

        //other example
        try {
            int divisibleByZero = 5 / 0;
            System.out.println("Rest of code in try block");
        } catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }


    }
}
