package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

//====Hashmap=====
        HashMap<String,Integer> gameStocks = new HashMap<>();

        gameStocks.put("Mario Odyssey", 50);
        gameStocks.put("Super Smash Bros. Ultimate",20);
        gameStocks.put("Luigi's Mansion 3", 15);
        gameStocks.put("Pokemon Sword", 30);
        gameStocks.put("Pokemon Shield", 100);
//        System.out.println(gameStocks);

        gameStocks.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left. ");
        });

//TopGames
        ArrayList<String> topGames = new ArrayList<>();

        gameStocks.forEach((key,value) -> {
            if(value <= 30){
                topGames.add(key);
                System.out.println( key + " has been added to top games list!");
                System.out.println( "Our top games: " );
                System.out.println(topGames);
            }
        });

//Stretch Goals
        boolean addItem = true;

        while(addItem){
            System.out.println("Would you like to add an item? Yes or No.");
            Scanner input = new Scanner(System.in);
            String userInput = input.nextLine();

            if(userInput.equals("Yes")){
                System.out.println("Add the item name");
                String itemName = input.nextLine();
                
                System.out.println("Add the item stock");
                int itemStock = input.nextInt();
                gameStocks.put(itemName, itemStock);
                System.out.println(itemName + " has been added with " + itemStock + " stocks");
                addItem = false;

            } else if (userInput.equals("No")){
                System.out.println("Thank you");
                addItem = false;

            } else {
                System.out.println("Invalid input. Try again.");
            }
        }



//        boolean addItem = true;
//        System.out.println("Would you like to add an item? Yes or No?");
//
//        while(addItem) {
//            Scanner itemScanner = new Scanner(System.in);
//
//            String userInput = itemScanner.nextLine();
//            if (userInput.equals("Yes")){
//
//            System.out.println("Add the item name");
//            String itemName = itemScanner.nextLine();
//            System.out.println("Add the item stock");
//            int itemStock = itemScanner.nextInt();
//
//            gameStocks.put(itemName, itemStock);
//            System.out.println(itemName + " has been added with " + itemStock + " stocks.");
//            addItem = false;
//
//            } else if ( userInput.equals("No")) {
//                break;
//                addItem = false;
//                System.out.println("Thank you");
//
//            } else
//                System.out.println("Invalid input. Try again.");


//        }
    }
}
